# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

# Shell timeout definition

TMOUT=600
readonly TMOUT
export TMOUT
