# SPDX-FileCopyrightText: Le Van Quach <levan.quach@kalpa.it>
#
# SPDX-License-Identifier: MIT

SUMMARY = "This library contains support code for many of the tests of the checkbox project."
HOMEPAGE = "https://launchpad.net/checkbox-support"

LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "git://git.launchpad.net/checkbox-support;protocol=https;branch=master"
SRCREV = "bb747ea122611e734e8a586e43ea0345bab84c08"

inherit setuptools3

S = "${WORKDIR}/git"
RDEPENDS:${PN} += " \
	${PYTHON_PN}-core \
        ${PYTHON_PN}-pyparsing \
        ${PYTHON_PN}-requests \
        ${PYTHON_PN}-distro \
        ${PYTHON_PN}-pybluez \
"
