.. SPDX-FileCopyrightText: Linaro Ltd
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: definitions.rst

.. _DefaultPassword:

Setting the Default Password
############################

|main_project_name| provides a default user with default password.
Device vendor should change this password for their devices and disable the remote
login if not needed.

The default password is configured by the variable **"ONIRO_USER_PASSWORD"**.
You can define in in your ``local.conf`` using a command like (replace the password
and the seed by your own values):

.. code-block:: console

    mkpasswd -m sha-512 <yourpassword> -s "seed"

Replace the seed and password with your own data, also make sure to escape all special shell
characters, the `%q` formating of shell `printf` is useful. The final command could be:

For example:

.. code-block:: console

    $ printf "%q" $(mkpasswd -m sha512crypt 123456 -s "seed1234" )
    \$6\$seed1234\$qd8wosDovm0k4OSCKGSbnwrj4SJf.92UyAqEgdE3xhD0tHxhqqoSxqmsjiXfj4ue0ih4txcKHQWqGQ.1HZmm7.

In this case, your ``local.conf`` (or an bbappend in one of your layers) should contain:

.. code-block:: console

    ONIRO_USER_PASSWORD="\$6\$seed1234\$qd8wosDovm0k4OSCKGSbnwrj4SJf.92UyAqEgdE3xhD0tHxhqqoSxqmsjiXfj4ue0ih4txcKHQWqGQ.1HZmm7."

After rebuilding your image, you should be able to login with the `123456` password.

.. Note::

   The password and seed in this document is shown for illustration purposed only. In a production
   sytem, use different values than the default ones, or what has been shown in this document.
